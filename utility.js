const chalk_pack = require('chalk');
const chalk = new chalk_pack.constructor({
    level: 4
});

var utility = {
    getSubject: function(message) {
        return this.findHeaderValueByName(message.payload.headers, "Subject");
    },

    getSender: function(message) {
        return this.findHeaderValueByName(message.payload.headers, "From");
    },

    findHeaderValueByName: function(headers, name) {
        for (let i = 0; i < headers.length; i++) {
            let header = headers[i];
            if (header.name == name) return header.value;
        }
        return false;
    },

    detectWebsite: function(message) {
        var mailFrom = this.getSender(message);

        if (mailFrom === false) {
            console.log("Mail does not have a 'From' header");
            return false;
        }else if(mailFrom.indexOf("mover.az") > -1){
            return "1v1y.com";
            // return "trendyol.com";
            // return "mover.az";
        }else if(mailFrom.indexOf("trendyol.com") > -1){
            return "trendyol.com";
        }else if(mailFrom.indexOf("1v1y.com") > -1){
            return "1v1y.com";
        }

        console.log("Website not detected: %s",mailFrom);
        return false;
    },



    mailIsAlreadyHandled: function(message) {
        /**
            Eger mail oxunubsa, ve ya mailde 'Status updated'(ID: 'Label_2') labeli varsa, true qaytarir.
            Hemin mail grabber terefinden ignore olunur.
        */
        var labels = message.labelIds; // id => 'Label_2', name => 'Status updated'

        if (labels.includes("Label_2") && !labels.includes("UNREAD")) {
            console.log(chalk.blue("Has already been handled: %s"), this.getSubject(message));
            return true;
        }

        return false;
    }
}

module.exports = utility;
