const request = require('request');
const sha256 = require('sha256');
const chalk_pack = require('chalk');
const chalk = new chalk_pack.constructor({
    level: 4
});

var moverApiKey = "delixana1";
var requestUrl = 'http://localhost/mover2/mailGrabber';

module.exports = {
    updateStatusOnDatabase: function(formData) {
        return new Promise(function(resolve, reject) {
            formData["apiKey"] = sha256(moverApiKey);
            var callback = function(err, res, body) {
                console.log("Id -> %s / St -> %s", formData.trackNumber, formData.status);
                if(err){
                    console.error(chalk.red("Something went wrong(%s): %s"),res.statusCode , err);
                    reject(err);
                }else if(res.statusCode != 200){
                    body = JSON.parse(body);

                    console.error(chalk.red("Mover API returned(%s) error: %s"),res.statusCode , body.error);
                    reject(body.error);
                }else{
                    console.log(chalk.green("SUCCESS: %s -> %s"),formData.trackNumber,formData.status);
                    resolve();
                }
            };

            request.post({
                url: requestUrl,
                headers: {
                    'User-Agent': 'request'
                },
                form: formData
            }, callback);
        });
    },

    sendInvoiceLink: function(formData) {
        return new Promise(function(resolve, reject) {
            formData["apiKey"] = sha256(moverApiKey);
            var callback = function(err, res, body) {
                console.log("Id -> %s / St -> %s", formData.trackNumber, formData.status);
                if (err) {
                    console.error(chalk.red("Something went wrong(%s): %s"),res.statusCode , err);
                    reject(err);
                }else if(res.statusCode != 200){
                    body = JSON.parse(body);

                    console.error(chalk.red("Mover API returned(%s) error: %s"),res.statusCode , body.error);
                    reject(body.error);
                }else{
                    console.log(chalk.green("SUCCESS: %s -> %s"),formData.trackNumber,formData.status);
                    resolve();
                }
            };

            request.post({
                url: requestUrl,
                headers: {
                    'User-Agent': 'request'
                },
                form: formData
            }, callback);
        });
    }
}
