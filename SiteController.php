public function actionMailGrabber(){
	    $apiKey  = 'delixana1';
        $trackNumber = isset($_POST["trackNumber"]) ? $_POST["trackNumber"] : NULL;
        $website = isset($_POST["website"]) ? $_POST["website"] : NULL;
        $invoiceLink = isset($_POST["invoiceLink"]) ? $_POST["invoiceLink"] : NULL;
        $status = isset($_POST["status"]) ? $_POST["status"] : NULL;

	    if(!isset($_POST["apiKey"]) || hash('sha256',$apiKey) != $_POST["apiKey"]){
            http_response_code(401);
            $this->sendJSONResponse(array(
                "error" => "Not authorized."
            ));
            Yii::app()->end();
        }

	    if($trackNumber && $invoiceLink && $website){
	        $this->addInvoiceFile($website,$trackNumber,$invoiceLink);
        }else if($trackNumber && $status){
            $this->updateOrderDeliveryStatus($trackNumber,$status);
        }else{
            http_response_code(400);

            $this->sendJSONResponse(array(
                "error" => "Not enough parameters sent"
            ));

            Yii::app()->end();
        }

//        $this->sendJSONResponse(array(
//            "success" => true
//        ));
        Yii::app()->end();
    }

    private function addInvoiceFile($website,$trackNumber,$invoiceLink){
        $criteria = new CDbCriteria;
        $criteria->condition = "tracking=:tracking";
        $criteria->params = array(':tracking' => $trackNumber);
        $order = Orders::model()->find($criteria);

        if($order == NULL){
            http_response_code(404);

            $this->sendJSONResponse(array(
                "error" => "An order with given track number doesn't exists"
            ));

            Yii::app()->end();
        }else if($order->file != NULL){
            http_response_code(403);

            $this->sendJSONResponse(array(
                "error" => "Invoice already exists"
            ));

            Yii::app()->end();
        }else if($website == "trendyol.com"){
            $fileName = round(microtime(true) * 1000).".pdf";
            $filePath = Yii::app()->params['invoice_path'];

            if (!file_exists($filePath)) {
                mkdir($filePath, 0777, true);
            }

            file_put_contents($filePath.$fileName, file_get_contents($invoiceLink));

            $order->file = $fileName;
            $order->save();
        }else{
            http_response_code(400);

            $this->sendJSONResponse(array(
                "error" => "Request parameters didn't match any condition"
            ));

            Yii::app()->end();
        }
    }

    private function updateOrderDeliveryStatus($trackNumber,$status){
        $criteria = new CDbCriteria;
        $criteria->condition = "tracking=:tracking";
        $criteria->params = array(':tracking' => $trackNumber);
        $order = Orders::model()->find($criteria);

        if($order == NULL){
            http_response_code(404);
            $this->sendJSONResponse(array(
                "error" => "An order with given track number doesn't exists"
            ));
            Yii::app()->end();
        }else{
            $deliveryStatus = $order["delivery"];

            if($status > 6){
                http_response_code(400);
                Yii::app()->end();
            }else if($status > $deliveryStatus){
                $order->delivery = $status;
                $order->save();
            }
        }
    }

    public function sendJSONResponse($arr)
    {
        header('Content-type: application/json');
        echo json_encode($arr);
        Yii::app()->end();
    }

    private function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
