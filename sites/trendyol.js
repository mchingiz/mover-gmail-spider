const base64url = require('base64-url');
const cheerio = require('cheerio');
const chalk_pack = require('chalk');
const chalk = new chalk_pack.constructor({level: 4});
var util = require('../utility');
var moverApi = require('../moverApi');

class Trendyol{
    constructor(message){
        this.message = message;
        this.html = base64url.decode(message.payload.parts[1].body.data);

        // console.log(chalk.yellow(this.html));
    }

    handle(){
        var that = this;
        var trackNumber = false;
        var status = false;
        var invoiceLink = false;
        var cargoTrackingLink = false;
        var totalPrice = false;

        return new Promise(function(resolve,reject){
            trackNumber = that.getTrackNumber();
            status = that.getStatus();
            invoiceLink = that.getInvoiceLink();
            cargoTrackingLink = that.getCargoTrackingLink();
            totalPrice = that.getTotalPrice();

            if(status == "sifaris alindi" && (trackNumber && status && !invoiceLink)){
                /**
                    Bir sifarisde bir nece mehsul varsa ola biler ki bir nece teslimat olaraq gonderilsin.
                    Belede her teslimatin nomresin alib, heresini ayri handle elemek lazimdi.
                    Ona gore mailin basligi "sifaris alindi"disa xususi hal kimi handle eleyerik.
                    Bu halda trackNumber teslimatlara yox sifarisin ozune aid olur deye ehemiyyet dasimir,yeni istifade olunmayacaq.

                    Numune maillerde 'siparisAlindi.html'-i acib bax.
                    trackNumber = 53130421 (butun sifarisin nomresi)
                    orderNumbers = [60410874,60410875] (Ayri mehsullarin nomresi)
                */
                var orderNumbers = that.getOrderNumbers();
                var promiseChain = new Array();

                for(var i=0;i<orderNumbers.length;i++){
                    promiseChain.push(
                        moverApi.updateStatusOnDatabase({
                            website: "trendyol.com",
                            trackNumber: orderNumbers[i],
                            status: status,
                        })
                    );
                }

                Promise.all(promiseChain)
                    .then(function() {
                        // console.log(chalk.green("Id -> %s / St -> %s"), trackNumber, status);
                        resolve();
                    })
                    .catch(function(err) {
                        console.error(chalk.red("Catch method of 'updateStatusOnDatabase' is called"));
                        reject(err);
                    });
            }else if(trackNumber && status && !invoiceLink){
                /**
                    'e-Arsiv faturasi' ve 'siparisAlindi'dan basqa butun trendyol mailleri bu if case-de handle olunur
                    moverApi-a gonderilen 'totalPrice' ve 'cargoTrackingLink' mailin mezmunundan asili olaraq false ola biler
                */
                moverApi.updateStatusOnDatabase({
                    website: "trendyol.com",
                    trackNumber: trackNumber,
                    status: status,
                    totalPrice: totalPrice, 
                    cargoTrackingLink: cargoTrackingLink,
                }).then(function() {
                        // console.log(chalk.green("Id -> %s / St -> %s"), trackNumber, status);
                        resolve();
                    })
                    .catch(function(err) {
                        console.error(chalk.red("Catch method of 'updateStatusOnDatabase' is called"));
                        reject(err);
                    });
            }else if(trackNumber && status == "invoiceLink" && invoiceLink){
                /**
                    Yalniz 'e-Arsiv faturasi' bu case-de handle olunur
                */
                moverApi.sendInvoiceLink({
                    website: "trendyol.com",
                    trackNumber: trackNumber,
                    invoiceLink: invoiceLink
                }).then(function() {
                    resolve();
                })
                .catch(function(err) {
                    console.error(chalk.red("Catch method of 'sendInvoiceLink' is called"));
                    reject(err);
                });
            }else{
                // not enough data
            }
        })
    }

    getStatus(){
        let subject = util.findHeaderValueByName(this.message.payload.headers,"Subject");

        if(subject.indexOf("Siparişiniz alındı") > -1){
            return "sifaris alindi";
        }else if(subject.indexOf("Siparişiniz kargoya verildi") > -1){
            return "sifaris karqoya verildi";
        }else if(subject.indexOf("Siparişiniz teslim edildi") > -1){
            return "sifarisiniz teslim edildi";
        }else if(subject.indexOf("Siparişinizin İptali Gerçekleştirildi") > -1){
            return "sifarisiniz iptali gerceklestirildi";
        }else if(subject.indexOf("E-Arşiv Faturası") > -1){
            return "invoiceLink";
        }else{
            console.log("Unknown mail subject. Will ignore. Id: %s / Subject: %s",this.message.id,subject);
            return false;
        }
    }

    getTrackNumber(){
        // Will match text parts which start with '#' and continues with 8 digits
        // var regex = new RegExp("(\\B\\#\\1)+(\\d[0-9]{7})");
        // var regex = new RegExp("(\\B\\#\\1)+(\\d[0-9]{7})");
        var regex = new RegExp("(\\d[0-9]{7})+(?=\\s+)");
        /*
            Yuxaridaki regexin exec funksiyasindan gele bilecek numune result:
            [
                '#60410874', //2 part birlikde
                '#', //1ci part
                '60410874', //2ci part
                index: 3640,
                input: gonderilenString
            ]
         */


        if(regex.test(this.html)){
            var trackNumber = regex.exec(this.html)[0];
            return trackNumber;
        }else{
            console.log("Regex not found");
            return false;
        }

    }

    getInvoiceLink(){
      var $ = cheerio.load(this.html);
      var link = $("img[alt='Faturaya Git']").closest("a").attr("href");

      return link;
    }

    getCargoTrackingLink(){
        var $ = cheerio.load(this.html);
        var link = $('a').filter(function(index) { return $(this).text() === "This One"; }).attr("href");

        return (link === "" ? false : link);
    }

    getTotalPrice(){
        var $ = cheerio.load(this.html);
        var price = $("span").filter(function(index) { return $(this).text() === "Toplam"; }).closest("tr").find("td:nth-child(2) span").text().replace(" tl","").trim();

        return (price == "" ? false : price);
    }

    getOrderNumbers(){
        var $ = cheerio.load(this.html);

        var elements = $("span").filter(function(index) {
            if($(this).text().indexOf("Teslimat No") > -1){
        		return true;
        	}else{
        		return false;
        	}
        })

        var orderNumbers = new Array();

        for(var i=0;i<elements.length;i++){
        	orderNumbers[i] = $(elements[i]).text().replace("(Teslimat No: ","").replace(")","").trim();
        }

        if(orderNumbers.length > 0){
            return orderNumbers;
        }else{
            return false;
        }
    }
}

module.exports = Trendyol;

// ASSUMPTIONS

// Track number on trendyol will always be 8 digit long
// Mail will not contain more than one track number
// Trendyol mails will come from trendyol.com
// Subject always will be in Turkish
