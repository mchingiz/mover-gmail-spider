const base64url = require('base64-url');
const chalk_pack = require('chalk');
const chalk = new chalk_pack.constructor({level: 4});
const cheerio = require('cheerio');
var util = require('../utility');
var moverApi = require('../moverApi');

class OneVOneY{
    constructor(message){
        this.message = message;
        this.html = base64url.decode(message.payload.parts[1].body.data);

        // console.log(this.html);
        // console.log("\n\n\n\n\n\n\n\n\n\n\n");

        // if(this.message.id == "15ef0c17965444a0"){
        //   console.log(chalk.green(this.html))
        // }
    }

    handle(){
        var that = this;
        var trackNumber = false;
        var status = false;
        var cargoTrackingLink = false;

        return new Promise(function(resolve,reject){
            trackNumber = that.getTrackNumber();
            status = that.getStatus();
            cargoTrackingLink = that.getTrackingLink();

            if(trackNumber && status){

                        console.log(chalk.green("Id -> %s / St -> %s / Link -> %s"), trackNumber, status, cargoTrackingLink);
                moverApi.updateStatusOnDatabase({
                    website: "1v1y.com",
                    trackNumber: trackNumber,
                    status: status,
                    cargoTrackingLink: cargoTrackingLink,
                }).then(function() {
                        // console.log(chalk.green("Id -> %s / St -> %s"), trackNumber, status);
                        resolve();
                    })
                    .catch(function(err) {
                        console.error(chalk.red("Catch method of 'updateStatusOnDatabase' is called"));
                        reject(err);
                    });
            }else{
                // not enough data
            }
        })
    }

    getStatus(){
        let subject = findHeaderValueByName(this.message.payload.headers,"Subject");

        if(subject.indexOf("Siparişin için teşekkür ederiz") > -1){
            return "Sifaris alindi";
        }else if(subject.indexOf("Siparişin yola çıkmaya hazır") > -1){
            return "sifaris karqoya verildi";
        }else{
            console.log("Unkown mail subject. Will ignore. Id: %s / Subject: %s",this.message.id,subject);
            return false;
        }
    }

    getTrackNumber(){
        // Will match text parts which start with 7 digits and is followed by whitespace
        var regex = new RegExp("(\\d[0-9]{6})+(?=\\s+)");
        /*
            Yuxaridaki regexin exec funksiyasindan gele bilecek numune result:
            [
                '7913554', //trackNumber
                input: // textin ozu
            ]
         */

        if(regex.test(this.html)){
            var trackNumber = regex.exec(this.html)[0];

            return trackNumber;
        }else{
            console.log("Regex not found");
            return false;
        }

    }

    getTrackingLink(){
        var $ = cheerio.load(this.html);
        var link = $("font")
            .filter(function(index){
    	       var text = $(this).text();

               if(text.indexOf("Siparişinin takibi için") > -1){
            		return true;
                }

                return false;
            }).find("a").attr("href");

        return (link === "" ? false : link);
    }
}

function findHeaderValueByName(headers,name){

    for(let i=0;i<headers.length;i++){
        let header = headers[i];

        if(header.name == name){
            // console.log("HEADER: %s --> %s",header.name,header.value);
            return header.value;
        }
    }

    return false;
}

module.exports = OneVOneY;

// ASSUMPTIONS

// Track number on ??????? will always be 8 digit long
// Mail will not contain more than one track number
// ??????? mails will come from ???????.com
// Subject always will be in Turkish
