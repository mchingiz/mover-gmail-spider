// Dependencies
const request = require('request');
const base64url = require('base64-url');
const sha256 = require('sha256');
const chalk_pack = require('chalk');
const chalk = new chalk_pack.constructor({
    level: 4
});

var Trendyol = require('./sites/trendyol');
var OneVOneY = require('./sites/OneVOneY');
var util = require('./utility');
var moverApi = require('./moverApi');
// const cheerio = require('cheerio');

var fs = require('fs');
var readline = require('readline');
var google = require('googleapis');
var googleAuth = require('google-auth-library');

// Global variables
// If you are modifying these scopes, delete your gmail-token.json
// Because you will need a new token for new scopes
var SCOPES = [
    'https://mail.google.com/'
];

var TOKEN_DIR = __dirname;
var TOKEN_PATH = TOKEN_DIR + 'gmail-token.json';
var authorizedUser;
var gmail = google.gmail('v1');


// ------------------------------
// ------- AUTHENTICATION -------
// ------------------------------

// Load client secrets from a local file.
fs.readFile('client_secret.json', function processClientSecrets(err, content) {
    if (err) {
        console.log('Error loading client secret file: ' + err);
        return;
    }
    // Authorize a client with the loaded credentials, then call the
    // Gmail API.
    authorize(JSON.parse(content), startGrabber);
});

/**
 * Create an OAuth2 client with the given credentials, and then execute the
 * given callback function.
 *
 * @param {Object} credentials The authorization client credentials.
 * @param {function} callback The callback to call with the authorized client.
 */
function authorize(credentials, callback) {
    var clientSecret = credentials.installed.client_secret;
    var clientId = credentials.installed.client_id;
    var redirectUrl = credentials.installed.redirect_uris[0];
    var auth = new googleAuth();
    var oauth2Client = new auth.OAuth2(clientId, clientSecret, redirectUrl);

    // Check if we have previously stored a token.
    fs.readFile(TOKEN_PATH, function(err, token) {
        if (err) {
            getNewToken(oauth2Client, callback);
        } else {
            oauth2Client.credentials = JSON.parse(token);
            authorizedUser = oauth2Client;
            callback(); // start point of application after authorization
        }
    });
}

/**
 * Get and store new token after prompting for user authorization, and then
 * execute the given callback with the authorized OAuth2 client.
 *
 * @param {google.auth.OAuth2} oauth2Client The OAuth2 client to get token for.
 * @param {getEventsCallback} callback The callback to call with the authorized
 *     client.
 */
function getNewToken(oauth2Client, callback) {
    var authUrl = oauth2Client.generateAuthUrl({
        access_type: 'offline',
        scope: SCOPES
    });
    console.log('Authorize this app by visiting this url: ', authUrl);
    var rl = readline.createInterface({
        input: process.stdin,
        output: process.stdout
    });
    rl.question('Enter the code from that page here: ', function(code) {
        rl.close();
        oauth2Client.getToken(code, function(err, token) {
            if (err) {
                console.log('Error while trying to retrieve access token', err);
                return;
            }
            oauth2Client.credentials = token;
            storeToken(token);
            callback(oauth2Client);
        });
    });
}

/**
 * Store token to disk be used in later program executions.
 *
 * @param {Object} token The token to store to disk.
 */
function storeToken(token) {
    try {
        fs.mkdirSync(TOKEN_DIR);
    } catch (err) {
        if (err.code != 'EEXIST') {
            throw err;
        }
    }
    fs.writeFile(TOKEN_PATH, JSON.stringify(token));
    console.log('Token stored to ' + TOKEN_PATH);
}

// -----------------------------
// ------- FUNCTIONALITY -------
// -----------------------------

function startGrabber(){
    var hours = 1;

    getMails();
    setInterval(getMails,hours*3600*1000);
}

function getMails() {
    gmail.users.messages.list({
        auth: authorizedUser,
        userId: 'me',
        labelIds: ['INBOX'],
        // q: 'trendyol'
        // q: '1V1Y'
        // q: 'e-arşiv'
        q: 'from:n.trendyol.com OR from:markalar.1v1y.com'
        // q: 'Siparişinizin İptali Gerçekleştirildi'
        // q: 'Siparişin için teşekkür ederiz'
        // q: 'Siparişin yola'
        // q: 'Siparişiniz teslim edildi'
        // q: 'Siparişiniz alındı'
    }, function(err, response) {
        if (err) {
            console.log('The getMails API returned an error: ' + err);
            return;
        }

        var messages = response.messages;

        if (messages.length == 0) {
            console.log('No messages found.');
        } else {
            console.log(chalk.bgGreen('Messages(%s):'), messages.length);

            for (var i = 0; i < messages.length; i++) {
                var message = messages[i];

                handleMail(message.id);
            }
        }
    });
}

function handleMail(id) {
    gmail.users.messages.get({
        auth: authorizedUser,
        userId: 'me',
        id: id,
        format: "full"
    }, function(err, response) {
        if (err) {
            console.log('The getMessage API returned an error: ' + err);
            return;
        }

        if (!util.mailIsAlreadyHandled(response)) {
            var website = util.detectWebsite(response);

            if (!website) {
                // Website not detected
            }else if(website == "trendyol.com"){
                var trendyolOrder = new Trendyol(response);

                trendyolOrder.handle()
                    .then(function(){
                        console.log('will update labels')
                        updateLabels(response);
                    })
                    .catch(function(err){
                        console.log(chalk.red("Trendyol handle returned error: %s"),err)
                    });

            }else if(website == "1v1y.com"){
                var OneVOneYOrder = new OneVOneY(response);

                OneVOneYOrder.handle()
                    .then(function(){
                        updateLabels(response);
                    })
                    .catch(function(err){
                        console.log(chalk.red("1v1y handle returned error: %s"),err)
                    });

            } else {
                // console.log(chalk.gray("Mail is from unknown website. Mail subject: %s"),getSubject(response));
            }
        }
    });
}

function updateLabels(message) {
    /**
        Maili oxunmus edirik ve 'Status updated' labelini elave edirik.
        Bu son prosesdi. Mailin tam handle olduguna emin olanda cagiririq
    */
    return true;
    var labelsToAdd = ["Label_2"];
    var labelsToRemove = ["UNREAD"];

    gmail.users.messages.modify({
        auth: authorizedUser,
        userId: 'me',
        id: message.id,
        resource: {
            addLabelIds: labelsToAdd, // adds 'Status updated' label
            removeLabelIds: labelsToRemove
        }
    }, function(err, response) {
        if (err) {
            console.error("Error updating labels: %s", err);
            return false;
        }

        console.log("Updated label");
    })
}
